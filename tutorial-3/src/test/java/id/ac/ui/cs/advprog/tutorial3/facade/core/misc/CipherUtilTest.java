package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.InfernalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CipherUtilTest {
    @Test
    public void testCipherUtilInstatiateWithList() throws Exception {
        List<Transformation> transformationList = new ArrayList<>();
        transformationList.add(new InfernalTransformation());
        transformationList.add(new AbyssalTransformation());
        transformationList.add(new CelestialTransformation());
        CipherUtil fooCipherUtil = new CipherUtil(transformationList);
        assertEquals("r?ixe:/po+pqimzbpL-r?", fooCipherUtil.encode("YOOOOOOOOOOOOOOOOOOOO"));
    }
}
