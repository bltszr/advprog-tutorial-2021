package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InfernalTransformationTest {
    private Class<?> infernalClass;

    @BeforeEach
    public void setup() throws Exception {
        infernalClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.InfernalTransformation");
    }

    @Test
    public void testInfernalHasEncodeMethod() throws Exception {
        Method translate = infernalClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }
    @Test
    public void testInfernalEncodesCorrectlyShortVer() throws Exception {
        String text = "HELLO";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "SVOOL";

        Spell result = new InfernalTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }


    @Test
    public void testInfernalEncodesCorrectly() throws Exception {
        String text = "666 million ways to die CHOOSE";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "666 nroorlm dzbh gl wrv XSLLHV";

        Spell result = new InfernalTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testInfernalHasDecodeMethod() throws Exception {
        Method translate = infernalClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testInfernalDecodesCorrectly() throws Exception {
        String text = "IRMTL NLTRIV yrrrrrRRRRNF";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "RINGO MOGIRE biiiiiIIIIMU";

        Spell result = new InfernalTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }
}
