package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.InfernalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.Transformation;

import java.util.ArrayList;
import java.util.List;

public class CipherUtil {
    private List<Transformation> transformationList;
    public CipherUtil() {
        // default implementation, Dante's Divine Comedy style
        this.transformationList = new ArrayList<>();
        transformationList.add(new CelestialTransformation());
        transformationList.add(new AbyssalTransformation());
        transformationList.add(new InfernalTransformation());
    }

    public CipherUtil(List<Transformation> transformationsList) {
        this.transformationList = transformationsList;
    }

    public String encode(String text) {
        Spell spell = new Spell(text, AlphaCodex.getInstance());
        for (Transformation transformation : transformationList) {
            spell = transformation.encode(spell);
        }
        return CodexTranslator.translate(spell, RunicCodex.getInstance()).getText();
    }

    public String decode(String code) {
        Spell spell = CodexTranslator.translate(
                new Spell(code, RunicCodex.getInstance()), AlphaCodex.getInstance());
        for (int i = transformationList.size() - 1; i >= 0; i-- ) {
            spell = transformationList.get(i).decode(spell);
        }
        return spell.getText();
    }
}
