package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;

// ignore the name, this is really really just atbash

public class InfernalTransformation implements Transformation {
    @Override
    public Spell encode(Spell spell) {
        return process(spell);
    }

    @Override
    public Spell decode(Spell spell) {
        return process(spell);
    }

    private Spell process(Spell spell) {
        Codex codex = spell.getCodex();
        String plainText = spell.getText();
        StringBuilder cipherText = new StringBuilder();
        for (int i = 0; i < plainText.length(); i++) {
            char candidate = plainText.charAt(i);
            if ('a' <= candidate && candidate <= 'z') {
                cipherText.append((char) ('a' + 'z' - candidate));
            } else if ('A' <= candidate && candidate <= 'Z') {
                cipherText.append((char) ('A' + 'Z' - candidate));
            } else {
                cipherText.append(candidate);
            }
        }
        return new Spell(cipherText.toString(), codex);
    }
}
