package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private BowRepository bowRepository;

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        List<Weapon> weaponsList;
        weaponsList = new ArrayList<>();

        weaponsList.addAll(weaponRepository.findAll());

        for (Bow bow : bowRepository.findAll()) {
            if (weaponRepository.findByAlias(bow.getName()) == null) {
                weaponsList.add(new BowAdapter(bow));
            }
            // else, do nothing, this weapon was already found above
            // I don't know if this case ever happens though
        }

        for (Spellbook spellbook : spellbookRepository.findAll()) {
            if (weaponRepository.findByAlias(spellbook.getName()) == null) {
                weaponsList.add(new SpellbookAdapter(spellbook));
            }
            // else, do nothing, this weapon was already found above
            // I don't know if this case ever happens though

        }
        return weaponsList;
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon;
        Spellbook spellbook;
        Bow bow;
        String attack, log;

        weapon = weaponRepository.findByAlias(weaponName);
        if (weapon == null) {
            // try again
            spellbook = spellbookRepository.findByAlias(weaponName);
            bow = bowRepository.findByAlias(weaponName);

            if (spellbook != null) {
                weapon = new SpellbookAdapter(spellbook);
            } else if (bow != null) {
                weapon = new BowAdapter(bow);
            } else {
                // no weapon, then
                return;
            }
        }
        // apparently, according to the tests, this new weapon must be
        // saved into weaponRepository
        // why? WHY SHOULD I KNOW LOL
        weaponRepository.save(weapon);

        switch (attackType) {
            case 1:
                attack = " (normal attack) " + weapon.normalAttack();
                break;
            case 2:
                attack = " (charged attack) " + weapon.chargedAttack();
                break;
            default:
                attack = " nothing ";
                break;
        }
        log = weapon.getHolderName() + " attacked with "
                + weapon.getName() + attack;

        logRepository.addLog(log);


    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
