package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private boolean cooldown;
    private Spellbook spellbook;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        this.cooldown = false;
    }

    @Override
    public String normalAttack() {
        this.cooldown = false;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if (cooldown) {
            return "Not enough mana to perform large spell!";
        } else {
            cooldown = true;
            return spellbook.largeSpell();
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
