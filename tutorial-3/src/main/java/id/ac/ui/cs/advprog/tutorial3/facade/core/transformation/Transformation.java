package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

// The idea of using the transformations as an interchangeable
// strategy is courtesy to Jonathan Amadeus (HardJoe)
public interface Transformation {
    Spell encode(Spell spell);
    Spell decode(Spell spell);
}
