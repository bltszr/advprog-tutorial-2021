package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me

    // Chain Spell. Gabungan beberapa spell yang dijalankan secara berurutan. Instruksi undo pada spell ini sedikit
    // berbeda, dimana chain spell melakukan undo pada setiap spell yang menyusunnya dengan urutan terbalik..

    // assumption: it has a list of spells?
    ArrayList<Spell> spellList;
    public ChainSpell(ArrayList<Spell> spellList) {
        this.spellList = spellList;
    }
    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast() {
        for (Spell spell : spellList) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = spellList.size() - 1; i >= 0; i--) {
            spellList.get(i).undo();
        }
    }
}
