package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public class FamiliarSummonSpell extends FamiliarSpell {
    public FamiliarSummonSpell(Familiar familiar) {
        super(familiar);
    }
    // TODO: Complete Me

    @Override
    public String spellName() {
        return familiar.getRace() + ":Summon";
    }

    @Override
    public void cast() {
        this.familiar.summon();
    }

    @Override
    public void undo() {
        super.undo();
    }
}
