package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    // the tests mentioned something "latestSpell", I can't tell if it's something
    // that is actually missing or if it's testing magic

    private Spell latestSpell;

    // we need this for undos done twice
    private Spell secondToLatestSpell;
    private Map<String, Spell> spells;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete Me
        this.secondToLatestSpell = this.latestSpell;
        this.latestSpell = spells.get(spellName);
        latestSpell.cast();
    }

    public void undoSpell() {
        // TODO: Complete Me
        if (latestSpell != null) latestSpell.undo();
        else return;
        Spell temp;
        temp = this.latestSpell;
        this.latestSpell = this.secondToLatestSpell;
        this.secondToLatestSpell = temp;

    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
