package id.ac.ui.cs.advprog.tutorial2.template.core;

import java.util.ArrayList;
import java.util.List;

public abstract class SpiritInQuest {

    public List attackPattern() {
        List list = new ArrayList();
        // TODO: Complete Me

        // I absolutely have zero clue what a good justification for this would be

        list.add(summon());
        list.add(getReady());
        // obviously, summon and get ready first

        list.add(buff());
        list.add(attackWithBuster());
        list.add(buff());
        list.add(attackWithQuick());
        list.add(buff());
        list.add(attackWithArts());
        // alternating attacks with buffs, because the buffs are different for every attack
        list.add(attackWithSpecialSkill());
        // special skills aren't buffed anywhere, so uh I guess it's just not buffed
        return list;
    }


    public String summon() {
        return "Summon a Spirit...";
    }

    public String getReady() {
        return "Spirit ready to enter the Quest";
    }

    protected abstract String buff();

    protected abstract String attackWithBuster();

    protected abstract String attackWithQuick();

    protected abstract String attackWithArts();

    protected abstract String attackWithSpecialSkill();
}
