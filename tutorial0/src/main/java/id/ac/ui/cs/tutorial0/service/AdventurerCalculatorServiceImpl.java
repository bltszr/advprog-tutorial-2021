package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        if (rawAge<30) {
            return rawAge*2000;
        } else if (rawAge <50) {
            return rawAge*2250;
        } else {
            return rawAge*5000;
        }
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }

    public String classifyPower(int powerLevel) {
        String classifiedPower;
        if (powerLevel > 100000) {
            classifiedPower = "A Class";
        } else if (powerLevel > 20000) {
            classifiedPower = "B Class";
        } else if (powerLevel > 0) {
            classifiedPower = "C Class";
        } else {
            // Technically this should throw an error, but let's leave it
            // as the default value for now
            classifiedPower = "Dalit Class";
        }
        return classifiedPower;
    }
}
