package id.ac.ui.cs.tutorial0.controller;

import id.ac.ui.cs.tutorial0.service.AdventurerCalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdventurerController {

    @Autowired
    private AdventurerCalculatorService adventurerCalculatorService;

    @RequestMapping("/adventurer/countPower")
    private String showAdventurerPowerFromBirthYear(@RequestParam("birthYear")int birthYear, Model model) {
        int powerLevel = adventurerCalculatorService.countPowerPotensialFromBirthYear(birthYear);
        model.addAttribute("power", powerLevel);

        /*
         power class is going to be a string just in case the classification will include words other than
         the classified power
         Why? Not sure
        */

        String powerClass = adventurerCalculatorService.classifyPower(powerLevel);
        model.addAttribute("class", powerClass);
        return "calculator";
    }
}
