package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {

    //ToDo: Complete me

    public KnightAdventurer() {
        // Knight Adventurer secara default memiliki attack behaviour
        // menggunakan Pedang dan defense behaviour Armor
        this.setAttackBehavior(new AttackWithSword());
        this.setDefenseBehavior(new DefendWithArmor());
    }
    @Override
    public String getAlias() {
        return "Knight";
    }
}
