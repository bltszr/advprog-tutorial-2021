package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        guild.getAdventurers().add(this);
        this.guild = guild;
        //ToDo: Complete Me
    }

    //ToDo: Complete Me

    @Override
    public void update() {
        Quest newQuest = guild.getQuest();
        // Rumble   : Adventurer Agile dan Knight saja yang menjalankan Quest Rumble.
        if (!"R".equals(guild.getQuestType())) {
            this.getQuests().add(newQuest);
        }
    }
}
