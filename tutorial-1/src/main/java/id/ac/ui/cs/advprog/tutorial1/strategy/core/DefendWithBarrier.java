package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    //ToDo: Complete me

    @Override
    public String defend() {
        return "萬里長城";
    }

    @Override
    public String getType() {
        return "Barrier";
    }
}
