package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
	
    //ToDo: Complete me

    public AgileAdventurer() {
        // Agile Adventurer secara default memiliki attack behaviour
        // menggunakan Senjata Api dan defense behaviour Penghadang(Barrier).
        this.setAttackBehavior(new AttackWithGun());
        this.setDefenseBehavior(new DefendWithBarrier());
    }

    @Override
    public String getAlias() {
        return "Agile";
    }
}
