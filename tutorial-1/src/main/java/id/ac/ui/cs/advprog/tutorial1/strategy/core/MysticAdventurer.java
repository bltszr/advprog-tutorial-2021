package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {

    //ToDo: Complete me
    public MysticAdventurer() {
        // Mystic Adventurer secara default memiliki attack
        // behaviour menggunakan Sihir dan defense behaviour menggunakan Perisai
        this.setAttackBehavior(new AttackWithMagic());
        this.setDefenseBehavior(new DefendWithShield());
    }
    @Override
    public String getAlias() {
        return "Mystic";
    }
}
