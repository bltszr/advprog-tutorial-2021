### Lazy vs Eager Instantiation
**Lazy Instantiation**: Ketika Singleton hanya diinstansiasi ketika dibutuhkan/dipanggil melalui `getInstance()` maupun 
method static lainnya.
- Pros: Lebih sedikit overhead jika memang Singleton tidak dibutuhkan sama sekali maupun untuk suatu porsi waktu yang
lama antara program load sampai Singleton tersebut dibutuhkan
- Cons: Jika memang ternyata dibutuhkan dari awal, maka pros di atas tidak berlaku, dan bahkan mungkin menambah lebih
  banyak overhead karena tiap method static akan perlu mengecek adanya instansi setiap kali dipanggil dan berpotensi 
  melakukan instansiasi dalam pemanggilan tersebut.

**Eager Instantiation**: Ketika Singleton diinstansiasikan tepat saat program load, baik ketika dibutuhkan maupun
tidak.
- Pros: Jika memang dibutuhkan dari awal mulainya load program dan selama mayoritas runtime, maka waktu instansiasi 
  akan dialokasikan pada boot/awal runtime alih-alih di tengah-tengah program sedang berjalan yang mungkin mengganggu
  performa program ataupun pengalaman user, dll.
- Cons: Jika tidak, pros di atas tidak berlaku, dan bahkan jika selama mayoritas runtime program Singleton bersifat
  idle,maka Singleton hanya akan menghabiskan memory yang bisa digunakan oleh bagian program lainnya, sehingga mungkin
  memperburuk performa program.
