package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class MondoUdonFactoryTest {
    private Class<?> mondoUdonFactoryClass;
    Flavor mockFlavor;
    Meat mockMeat;
    Noodle mockNoodle;
    Topping mockTopping;
    MenuFactory testSubject;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonFactory");

    }

    @Test
    public void testMondoUdonIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mondoUdonFactoryClass.getModifiers()));
    }

    @Test
    public void testMondoUdonIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(mondoUdonFactoryClass.getInterfaces());
        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory")));
    }

    @Test
    public void testMondoUdonOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = mondoUdonFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }
    @Test
    public void testMondoUdonOverrideCreateMeatMethod() throws Exception {
        Method createMeat = mondoUdonFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testMondoUdonOverrideNCreateNoodleMethod() throws Exception {
        Method createNoodle = mondoUdonFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testMondoUdonOverrideCreateToppingMethod() throws Exception {
        Method createTopping = mondoUdonFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }
    @Test
    public void testMondoUdonOverriddenMethodsAreCorrect() {

        //Noodle: Udon
        mockNoodle = new Udon();
        //Meat: Chicken
        mockMeat = new Chicken();
        //Topping: Cheese
        mockTopping = new Cheese();
        //Flavor: Salty
        mockFlavor = new Salty();

        testSubject = new MondoUdonFactory();
        assertEquals(testSubject.createNoodle().getDescription(), mockNoodle.getDescription());
        assertEquals(testSubject.createMeat().getDescription(), mockMeat.getDescription());
        assertEquals(testSubject.createTopping().getDescription(), mockTopping.getDescription());
        assertEquals(testSubject.createFlavor().getDescription(), mockFlavor.getDescription());



    }
}
