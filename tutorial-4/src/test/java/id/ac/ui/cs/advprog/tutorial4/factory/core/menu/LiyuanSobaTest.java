package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class LiyuanSobaTest {
    private Class<?> liyuanSobaClass;
    private Flavor mockFlavor;
    private Meat mockMeat;
    private Noodle mockNoodle;
    private Topping mockTopping;
    private Menu testSubject;
    private String testSubjectName;

    @BeforeEach
    public void setUp() throws Exception {
        liyuanSobaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");

    }

    @Test
    public void testLiyuanSobaIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(liyuanSobaClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIsAMenu() {
        Class<?> superclass = liyuanSobaClass.getSuperclass();
        assertEquals(superclass.getTypeName(), "id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testLiyuanSobaOverrideGetFlavorMethod() throws Exception {
        Method getFlavor = liyuanSobaClass.getMethod("getFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                getFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(getFlavor.getModifiers()));
    }
    @Test
    public void testLiyuanSobaOverrideGetMeatMethod() throws Exception {
        Method getMeat = liyuanSobaClass.getMethod("getMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
    }

    @Test
    public void testLiyuanSobaOverrideNGetNoodleMethod() throws Exception {
        Method getNoodle = liyuanSobaClass.getMethod("getNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(getNoodle.getModifiers()));
    }

    @Test
    public void testLiyuanSobaOverrideGetToppingMethod() throws Exception {
        Method getTopping = liyuanSobaClass.getMethod("getTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
    }
    @Test
    public void testLiyuanSobaOverriddenMethodsAreCorrect() {

        //Noodle: Soba
        mockNoodle = new Soba();
        //Meat: Beef
        mockMeat = new Beef();
        //Topping: Mushroom
        mockTopping = new Mushroom();
        //Flavor: Sweet
        mockFlavor = new Sweet();
        testSubjectName = "WanPlus Beef Mushroom Soba";

        testSubject = new LiyuanSoba(testSubjectName);
        assertEquals(testSubject.getNoodle().getDescription(), mockNoodle.getDescription());
        assertEquals(testSubject.getMeat().getDescription(), mockMeat.getDescription());
        assertEquals(testSubject.getTopping().getDescription(), mockTopping.getDescription());
        assertEquals(testSubject.getFlavor().getDescription(), mockFlavor.getDescription());
        assertEquals(testSubject.getName(), testSubjectName);


    }
}
