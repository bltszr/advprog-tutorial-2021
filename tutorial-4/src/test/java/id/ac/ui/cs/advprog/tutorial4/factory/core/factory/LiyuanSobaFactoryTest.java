package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class LiyuanSobaFactoryTest {
    private Class<?> LiyuanSobaFactoryClass;
    Flavor mockFlavor;
    Meat mockMeat;
    Noodle mockNoodle;
    Topping mockTopping;
    MenuFactory testSubject;

    @BeforeEach
    public void setUp() throws Exception {
        LiyuanSobaFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaFactory");

    }

    @Test
    public void testLiyuanSobaIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(LiyuanSobaFactoryClass.getModifiers()));
    }

    @Test
    public void testLiyuanSobaIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(LiyuanSobaFactoryClass.getInterfaces());
        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory")));
    }

    @Test
    public void testLiyuanSobaOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = LiyuanSobaFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }
    @Test
    public void testLiyuanSobaOverrideCreateMeatMethod() throws Exception {
        Method createMeat = LiyuanSobaFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testLiyuanSobaOverrideNCreateNoodleMethod() throws Exception {
        Method createNoodle = LiyuanSobaFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testLiyuanSobaOverrideCreateToppingMethod() throws Exception {
        Method createTopping = LiyuanSobaFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }
    @Test
    public void testLiyuanSobaOverriddenMethodsAreCorrect() {
        //Noodle: Soba
        mockNoodle = new Soba();
        //Meat: Beef
        mockMeat = new Beef();
        //Topping: Mushroom
        mockTopping = new Mushroom();
        //Flavor: Sweet
        mockFlavor = new Sweet();

        testSubject = new LiyuanSobaFactory();
        assertEquals(testSubject.createNoodle().getDescription(), mockNoodle.getDescription());
        assertEquals(testSubject.createMeat().getDescription(), mockMeat.getDescription());
        assertEquals(testSubject.createTopping().getDescription(), mockTopping.getDescription());
        assertEquals(testSubject.createFlavor().getDescription(), mockFlavor.getDescription());



    }
}
