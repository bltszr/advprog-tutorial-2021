package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MenuServiceImplTest {
    private Class<?> menuServiceImplClass;

    @Mock
    MenuRepository menuRepository;

    @InjectMocks
    MenuService menuServiceImpl = new MenuServiceImpl();

    @BeforeEach
    public void setUp() throws Exception {
        menuServiceImplClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl");
    }


    @Test
    public void testMenuServiceImplHasImplementedMethods() throws Exception {
        Method getMenus = menuServiceImplClass.getMethod("getMenus");
        int methodModifiers = getMenus.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));

        Method createMenu = menuServiceImplClass.getMethod("createMenu", String.class, String.class);
        methodModifiers = createMenu.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
        assertFalse(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testGetMenusWorksCorrectly() throws Exception {
        List<Menu> menuList = new ArrayList<>();
        when(menuRepository.getMenus()).thenReturn(menuList);
        assertEquals(menuServiceImpl.getMenus(), menuList);
    }

    @Test
    public void testCreateMenuWorksCorrectly() throws Exception {
        MenuRepository iHaveNoIdeaHowMockitoWorks = new MenuRepository();
        menuServiceImpl = new MenuServiceImpl(iHaveNoIdeaHowMockitoWorks);
        int previousMenuSize = menuServiceImpl.getMenus().size();
        menuServiceImpl.createMenu("Neko Ramen With Akashi","InuzumaRamen");

        assertEquals(InuzumaRamen.class, menuServiceImpl.getMenus()
                .get(previousMenuSize).getClass());
        assertEquals(previousMenuSize + 1, menuServiceImpl.getMenus().size());
    }
}
