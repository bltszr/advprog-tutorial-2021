package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class InuzumaRamenTest {
    private Class<?> InuzumaRamenClass;
    private Flavor mockFlavor;
    private Meat mockMeat;
    private Noodle mockNoodle;
    private Topping mockTopping;
    private Menu testSubject;
    private String testSubjectName;

    @BeforeEach
    public void setUp() throws Exception {
        InuzumaRamenClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");

    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(InuzumaRamenClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIsAMenu() {
        Class<?> superclass = InuzumaRamenClass.getSuperclass();
        assertEquals(superclass.getTypeName(), "id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testInuzumaRamenOverrideGetFlavorMethod() throws Exception {
        Method getFlavor = InuzumaRamenClass.getMethod("getFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                getFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(getFlavor.getModifiers()));
    }
    @Test
    public void testInuzumaRamenOverrideGetMeatMethod() throws Exception {
        Method getMeat = InuzumaRamenClass.getMethod("getMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
    }

    @Test
    public void testInuzumaRamenOverrideNGetNoodleMethod() throws Exception {
        Method getNoodle = InuzumaRamenClass.getMethod("getNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(getNoodle.getModifiers()));
    }

    @Test
    public void testInuzumaRamenOverrideGetToppingMethod() throws Exception {
        Method getTopping = InuzumaRamenClass.getMethod("getTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
    }
    @Test
    public void testInuzumaRamenOverriddenMethodsAreCorrect() {

        //Noodle: Ramen
        mockNoodle = new Ramen();
        //Meat: Pork
        mockMeat = new Pork();
        //Topping: Boiled Egg
        mockTopping = new BoiledEgg();
        //Flavor: Spicy
        mockFlavor = new Spicy();
        testSubjectName = "Bakufu Spicy Pork Ramen";

        testSubject = new InuzumaRamen(testSubjectName);
        assertEquals(testSubject.getNoodle().getDescription(), mockNoodle.getDescription());
        assertEquals(testSubject.getMeat().getDescription(), mockMeat.getDescription());
        assertEquals(testSubject.getTopping().getDescription(), mockTopping.getDescription());
        assertEquals(testSubject.getFlavor().getDescription(), mockFlavor.getDescription());
        assertEquals(testSubject.getName(), testSubjectName);


    }
}
