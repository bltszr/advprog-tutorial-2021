package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class InuzumaRamenFactoryTest {
    private Class<?> inuzumaRamenFactoryClass;
    Flavor mockFlavor;
    Meat mockMeat;
    Noodle mockNoodle;
    Topping mockTopping;
    MenuFactory testSubject;

    @BeforeEach
    public void setUp() throws Exception {
        inuzumaRamenFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenFactory");

    }

    @Test
    public void testInuzumaRamenIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(inuzumaRamenFactoryClass.getModifiers()));
    }

    @Test
    public void testInuzumaRamenIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(inuzumaRamenFactoryClass.getInterfaces());
        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory")));
    }

    @Test
    public void testInuzumaRamenOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = inuzumaRamenFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }
    @Test
    public void testInuzumaRamenOverrideCreateMeatMethod() throws Exception {
        Method createMeat = inuzumaRamenFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testInuzumaRamenOverrideNCreateNoodleMethod() throws Exception {
        Method createNoodle = inuzumaRamenFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testInuzumaRamenOverrideCreateToppingMethod() throws Exception {
        Method createTopping = inuzumaRamenFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }
    @Test
    public void testInuzumaRamenOverriddenMethodsAreCorrect() {
        //Noodle: Ramen
        mockNoodle = new Ramen();
        //Meat: Pork
        mockMeat = new Pork();
        //Topping: Boiled Egg
        mockTopping = new BoiledEgg();
        //Flavor: Spicy
        mockFlavor = new Spicy();

        testSubject = new InuzumaRamenFactory();
        assertEquals(testSubject.createNoodle().getDescription(), mockNoodle.getDescription());
        assertEquals(testSubject.createMeat().getDescription(), mockMeat.getDescription());
        assertEquals(testSubject.createTopping().getDescription(), mockTopping.getDescription());
        assertEquals(testSubject.createFlavor().getDescription(), mockFlavor.getDescription());



    }
}
