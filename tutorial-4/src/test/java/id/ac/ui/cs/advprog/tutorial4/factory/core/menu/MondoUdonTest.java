package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class MondoUdonTest {
    private Class<?> mondoUdonClass;
    private Flavor mockFlavor;
    private Meat mockMeat;
    private Noodle mockNoodle;
    private Topping mockTopping;
    private Menu testSubject;
    private String testSubjectName;

    @BeforeEach
    public void setUp() throws Exception {
        mondoUdonClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");

    }

    @Test
    public void testMondoUdonIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(mondoUdonClass.getModifiers()));
    }

    @Test
    public void testMondoUdonIsAMenu() {
        Class<?> superclass = mondoUdonClass.getSuperclass();
        assertEquals(superclass.getTypeName(), "id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testMondoUdonOverrideGetFlavorMethod() throws Exception {
        Method getFlavor = mondoUdonClass.getMethod("getFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                getFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(getFlavor.getModifiers()));
    }
    @Test
    public void testMondoUdonOverrideGetMeatMethod() throws Exception {
        Method getMeat = mondoUdonClass.getMethod("getMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
    }

    @Test
    public void testMondoUdonOverrideNGetNoodleMethod() throws Exception {
        Method getNoodle = mondoUdonClass.getMethod("getNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(getNoodle.getModifiers()));
    }

    @Test
    public void testMondoUdonOverrideGetToppingMethod() throws Exception {
        Method getTopping = mondoUdonClass.getMethod("getTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
    }
    @Test
    public void testMondoUdonOverriddenMethodsAreCorrect() {

        //Noodle: Udon
        mockNoodle = new Udon();
        //Meat: Chicken
        mockMeat = new Chicken();
        //Topping: Cheese
        mockTopping = new Cheese();
        //Flavor: Salty
        mockFlavor = new Salty();
        testSubjectName = "Good Hunter Cheese Chicken Udon";

        testSubject = new MondoUdon(testSubjectName);
        assertEquals(testSubject.getNoodle().getDescription(), mockNoodle.getDescription());
        assertEquals(testSubject.getMeat().getDescription(), mockMeat.getDescription());
        assertEquals(testSubject.getTopping().getDescription(), mockTopping.getDescription());
        assertEquals(testSubject.getFlavor().getDescription(), mockFlavor.getDescription());
        assertEquals(testSubject.getName(), testSubjectName);


    }
}
