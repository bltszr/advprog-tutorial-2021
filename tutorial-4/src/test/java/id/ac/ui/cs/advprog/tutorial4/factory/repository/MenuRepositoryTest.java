package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class MenuRepositoryTest {

    @Mock
    private List<Menu> menus;

    @InjectMocks
    private MenuRepository menuRepository;

    @BeforeEach
    public void setUp() {
        menuRepository = new MenuRepository();
        // menus = new ArrayList<>();
    }

    @Test
    public void testMenuRepoGetMenus() {
        ReflectionTestUtils.setField(menuRepository, "list", menus);
        assertEquals(menuRepository.getMenus(), menus);
    }

    @Test
    public void testMenuAdd() {
        Menu fooMenu = new LiyuanSoba("Karameru Tokisoba");
        menuRepository.add(fooMenu);
        assertEquals(1, menuRepository.getMenus().size());
        assertEquals((new ArrayList<Menu>(Arrays.asList(fooMenu))), menuRepository.getMenus());
    }
}
