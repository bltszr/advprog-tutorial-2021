package id.ac.ui.cs.advprog.tutorial4.factory.core.factory;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class SnevnezhaShiratakiFactoryTest {
    private Class<?> snevnezhaShiratakiFactoryClass;
    Flavor mockFlavor;
    Meat mockMeat;
    Noodle mockNoodle;
    Topping mockTopping;
    MenuFactory testSubject;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiFactory");

    }

    @Test
    public void testSnevnezhaShiratakiIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(snevnezhaShiratakiFactoryClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIsAMenuFactory() {
        Collection<Type> interfaces = Arrays.asList(snevnezhaShiratakiFactoryClass.getInterfaces());
        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MenuFactory")));
    }

    @Test
    public void testSnevnezhaShiratakiOverrideCreateFlavorMethod() throws Exception {
        Method createFlavor = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                createFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                createFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(createFlavor.getModifiers()));
    }
    @Test
    public void testSnevnezhaShiratakiOverrideCreateMeatMethod() throws Exception {
        Method createMeat = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                createMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                createMeat.getParameterCount());
        assertTrue(Modifier.isPublic(createMeat.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiOverrideNCreateNoodleMethod() throws Exception {
        Method createNoodle = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                createNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                createNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(createNoodle.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiOverrideCreateToppingMethod() throws Exception {
        Method createTopping = snevnezhaShiratakiFactoryClass.getDeclaredMethod("createTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                createTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                createTopping.getParameterCount());
        assertTrue(Modifier.isPublic(createTopping.getModifiers()));
    }
    @Test
    public void testSnevnezhaShiratakiOverriddenMethodsAreCorrect() {
        //Noodle: Shirataki
        mockNoodle = new Shirataki();
        //Meat: Fish
        mockMeat = new Fish();
        //Topping: Flower
        mockTopping = new Flower();
        //Flavor: Umami
        mockFlavor = new Umami();

        testSubject = new SnevnezhaShiratakiFactory();
        assertEquals(testSubject.createNoodle().getDescription(), mockNoodle.getDescription());
        assertEquals(testSubject.createMeat().getDescription(), mockMeat.getDescription());
        assertEquals(testSubject.createTopping().getDescription(), mockTopping.getDescription());
        assertEquals(testSubject.createFlavor().getDescription(), mockFlavor.getDescription());



    }
}
