package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class OrderServiceImplTest {
    private OrderService orderService;
    
    @BeforeEach
    public void setUp() throws Exception {
        orderService = new OrderServiceImpl();
    }

    @Test
    public void testSingletonsAreNotNull() {
        assertNotNull(orderService.getDrink());
        assertNotNull(orderService.getFood());
    }

    @Test
    public void testGetInstancesShouldReturnTheSameThing() {
        OrderDrink firstDrink = orderService.getDrink();
        OrderDrink secondDrink = orderService.getDrink();
        assertEquals(firstDrink, secondDrink);

        OrderFood firstFood = orderService.getFood();
        OrderFood secondFood = orderService.getFood();
        assertEquals(firstFood, secondFood);
    }

    @Test
    public void testReplacementTexts() {
        String foo = "Bababooey";

        orderService.orderADrink(foo);
        orderService.orderAFood(foo);
        assertEquals(foo, orderService.getDrink().getDrink());
        assertEquals(foo, orderService.getFood().getFood());
    }
}
