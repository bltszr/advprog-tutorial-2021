package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class SnevnezhaShiratakiTest {
    private Class<?> snevnezhaShiratakiClass;
    private Flavor mockFlavor;
    private Meat mockMeat;
    private Noodle mockNoodle;
    private Topping mockTopping;
    private Menu testSubject;
    private String testSubjectName;

    @BeforeEach
    public void setUp() throws Exception {
        snevnezhaShiratakiClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");

    }

    @Test
    public void testSnevnezhaShiratakiIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(snevnezhaShiratakiClass.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiIsAMenu() {
        Class<?> superclass = snevnezhaShiratakiClass.getSuperclass();
        assertEquals(superclass.getTypeName(), "id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu");
    }

    @Test
    public void testSnevnezhaShiratakiOverrideGetFlavorMethod() throws Exception {
        Method getFlavor = snevnezhaShiratakiClass.getMethod("getFlavor");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor",
                getFlavor.getGenericReturnType().getTypeName());
        assertEquals(0,
                getFlavor.getParameterCount());
        assertTrue(Modifier.isPublic(getFlavor.getModifiers()));
    }
    @Test
    public void testSnevnezhaShiratakiOverrideGetMeatMethod() throws Exception {
        Method getMeat = snevnezhaShiratakiClass.getMethod("getMeat");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat",
                getMeat.getGenericReturnType().getTypeName());
        assertEquals(0,
                getMeat.getParameterCount());
        assertTrue(Modifier.isPublic(getMeat.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiOverrideNGetNoodleMethod() throws Exception {
        Method getNoodle = snevnezhaShiratakiClass.getMethod("getNoodle");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle",
                getNoodle.getGenericReturnType().getTypeName());
        assertEquals(0,
                getNoodle.getParameterCount());
        assertTrue(Modifier.isPublic(getNoodle.getModifiers()));
    }

    @Test
    public void testSnevnezhaShiratakiOverrideGetToppingMethod() throws Exception {
        Method getTopping = snevnezhaShiratakiClass.getMethod("getTopping");

        assertEquals("id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping",
                getTopping.getGenericReturnType().getTypeName());
        assertEquals(0,
                getTopping.getParameterCount());
        assertTrue(Modifier.isPublic(getTopping.getModifiers()));
    }
    @Test
    public void testSnevnezhaShiratakiOverriddenMethodsAreCorrect() {

        //Noodle: Shirataki
        mockNoodle = new Shirataki();
        //Meat: Fish
        mockMeat = new Fish();
        //Topping: Flower
        mockTopping = new Flower();
        //Flavor: Umami
        mockFlavor = new Umami();
        testSubjectName = "Morepeko Flower Fish Shirataki";

        testSubject = new SnevnezhaShirataki(testSubjectName);
        assertEquals(testSubject.getNoodle().getDescription(), mockNoodle.getDescription());
        assertEquals(testSubject.getMeat().getDescription(), mockMeat.getDescription());
        assertEquals(testSubject.getTopping().getDescription(), mockTopping.getDescription());
        assertEquals(testSubject.getFlavor().getDescription(), mockFlavor.getDescription());
        assertEquals(testSubject.getName(), testSubjectName);


    }
}

