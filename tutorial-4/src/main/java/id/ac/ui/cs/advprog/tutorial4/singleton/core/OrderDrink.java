package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import java.lang.Thread;

public class OrderDrink {

    // placeholder string
    private String drink = "No Order yet. Order first.";

    private static OrderDrink orderDrink = null;

    private OrderDrink() {
        try{
            System.out.println("Creating.....");
            Thread.sleep(2000);
            System.out.println("Done.....");
        } catch(InterruptedException e){
            e.printStackTrace();
        }
    }


    //Todo : Complete Me with lazy instantiation approach
    public static OrderDrink getInstance() {
        if (orderDrink == null) {
            orderDrink = new OrderDrink();
        }
        return orderDrink;
    }

    public String getDrink() {
        return this.drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    @Override
    public String toString() {
        return drink;
    }
}
